# seascape_course_cloud

# conda

## Install conda

Download and install conda following instructions [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)

Reopen your terminal and write the following line :

```
conda config --set auto_activate_base false
```

## Create seascape genomics conda environment 

Create your new environment with all required softwares for the seascape class:

```
conda env create -f env_seascape.yaml
```

activate conda environment

```
conda activate seascape
```

## Softwares

* [plink2 v2]()
* [admixture 1.3.0]()
* [VCFtools 0.1.16]()


# container 

